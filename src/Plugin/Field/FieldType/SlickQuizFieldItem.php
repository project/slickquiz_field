<?php

namespace Drupal\slickquiz_field\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\StringLongItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * SlickQuiz field type.
 *
 * @FieldType(
 *   id = "slickquiz",
 *   label = @Translation("SlickQuiz"),
 *   module = "slickquiz_field",
 *   description = @Translation("Allow create slick quiz."),
 *   default_widget = "slickquiz_json_form",
 *   default_formatter = "slickquiz_default"
 * )
 */
class SlickQuizFieldItem extends StringLongItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = parent::defaultFieldSettings() + [
      'completionResponseMessaging'  => FALSE,
      'perQuestionResponseMessaging' => FALSE,
      'skipStartButton'              => TRUE,
      'scoreAsPercentage'            => FALSE,
      'preventUnanswered'            => FALSE,
    ];
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);

    $settings = $this->getSettings();
    $element['completionResponseMessaging'] = [
      '#title' => $this->t('Completion response messaging'),
      '#description' => $this->t('Displays all questions and answers with correct or incorrect response messages when the quiz is completed.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['completionResponseMessaging'],
    ];

    $element['perQuestionResponseMessaging'] = [
      '#title' => $this->t('Per question response messaging'),
      '#description' => $this->t('Displays correct / incorrect response messages after each question is submitted.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['perQuestionResponseMessaging'],
    ];

    $element['skipStartButton'] = [
      '#title' => $this->t('Skip start button'),
      '#description' => $this->t('Whether or not to skip the quiz "start" button.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['skipStartButton'],
    ];

    $element['scoreAsPercentage'] = [
      '#title' => $this->t('Score as percentage'),
      '#description' => $this->t('Returns the score as a percentage rather than the number of correct responses.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['scoreAsPercentage'],
    ];

    $element['preventUnanswered'] = [
      '#title' => $this->t('Prevent unanswered'),
      '#description' => $this->t('Prevents submitting a question with zero answers.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['preventUnanswered'],
    ];

    return $element;
  }

}
