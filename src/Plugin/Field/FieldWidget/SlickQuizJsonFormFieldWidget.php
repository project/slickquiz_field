<?php

namespace Drupal\slickquiz_field\Plugin\Field\FieldWidget;

use Drupal\json_form\Plugin\Field\FieldWidget\JsonFormWidget;

/**
 * Plugin implementation of the 'text_default' formatter.
 *
 * @FieldWidget(
 *   id = "slickquiz_json_form",
 *   module = "slickquiz_field",
 *   label = @Translation("Slick quiz"),
 *   field_types = {
 *     "slickquiz",
 *   }
 * )
 */
class SlickQuizJsonFormFieldWidget extends JsonFormWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    $settings['json_form'] = '{"$schema":"http://json-schema.org/draft-03/schema#","type":"object","properties":{"info":{"type":"object","title":"Information","properties":{"level1":{"type":"string","title":"Message level 1 (80-100%)"},"level2":{"type":"string","title":"Message level 2 (60-79%)"},"level3":{"type":"string","title":"Message level 3 (40-59%)"},"level4":{"type":"string","title":"Message level 4 (20-39%)"},"level5":{"type":"string","title":"Message level 5 (0-19%)"}}},"questions":{"type":"array","title":"Questions","minItems":1,"uniqueItems":true,"items":{"type":"object","properties":{"q":{"type":"string","title":"Question","description":"Input the question text","required":true},"correct":{"type":"string","title":"Correct answer (message)"},"incorrect":{"type":"string","title":"Incorrect answer (message)"},"a":{"type":"array","title":"Question options","items":{"type":"object","properties":{"option":{"type":"string","title":"Answer","required":true},"correct":{"type":"boolean","required":true,"title":"Correct"}}}}}}}}}';
    $settings['json_form_default_value'] = '{"info":{"level1":"Very good","level2":"Try again","level3":"Try again","level4":"Try again","level5":"Try again"},"questions":[{"q":"¿Which is the correct answer?","correct":"Correct answer","incorrect":"Incorrect answer","a":[{"option":"A incorrect answer","correct":false},{"option":"A correct answer","correct":true},{"option":"Another correct answer","correct":false}]}]}';
    return $settings;
  }

}
