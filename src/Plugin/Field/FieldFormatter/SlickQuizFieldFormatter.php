<?php

namespace Drupal\slickquiz_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'text_default' formatter.
 *
 * @FieldFormatter(
 *   id = "slickquiz_default",
 *   module = "slizquiz_field",
 *   label = @Translation("Slick quiz"),
 *   field_types = {
 *     "slickquiz",
 *   }
 * )
 */
class SlickQuizFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $field_name = $this->fieldDefinition->getName();
    $entity_type = $this->fieldDefinition->getTargetEntityTypeId();
    $entity_id = $items->getEntity()->id();
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'slickquiz',
        '#entity_type' => $entity_type,
        '#entity_id' => $entity_id,
        '#field_name' => $field_name,
        '#delta' => $delta,
        '#attached' => [
          'library' => [
            'slickquiz_field/slickquiz',
            'slickquiz_field/slickquiz.quiz',
          ],
          'drupalSettings' =>
            [
              'slickQuizFieldSettings' => [
                $field_name => $this->getFieldSettings(),
              ],
              'slickQuizFieldInstances' => [
                $entity_type => [
                  $entity_id => [
                    $field_name => [
                      $delta => [
                        'json'       => json_decode($item->value),
                        'field_name' => $field_name,
                        'delta'      => $delta,
                      ],
                    ],
                  ],
                ],
              ],
            ],
        ],
      ];
    }

    return $elements;
  }

}
