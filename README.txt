This module allows you to create a field to generate quizzes
based on SlickQuiz library (demo here) and JSON form module.

Installation:
1. Install module in your site.
2. Download SlickQuiz library and place in: libraries/SlickQuiz

Usage:
1. In your content type 'Manage fields page'
   (or another entity type), add a field type named SlickQuiz.
2. (Optional) Configure SlickQuiz quiz structure default value and options.
3. In node add form (or another entity creation form) 
   you can start to make your quiz: add questions/ answers, different messages
   according leveling, etc. After save, you will have your quiz working.
