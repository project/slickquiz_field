/**
 * @file
 * Slick quiz implementation.
 */
(function ($, Drupal) {

  'use strict';

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.SlickQuiz = {
    attach: function (context, settings) {
      try {
        buildSlickQuizInstances(settings.slickQuizFieldInstances, settings.slickQuizFieldSettings, context);
      }
      catch(e) {
        console.error(e);
      }
    }
  };

  /**
   * Build slick quiz instances.
   *
   * @param array instances
   *   Slick quiz instances for each entity, all classified by entity type.
   * @param array context
   *   Context.
   */
  function buildSlickQuizInstances(instances, fieldSettings, context) {
    $.each(instances, function(entity_type, entities) {
      $.each(entities, function(entity_id, fields) {
        $.each(fields, function(field_name, field_values) {
          $.each(field_values, function(delta, slickQuizSettings) {
            var selector = '#slickQuiz-field-' + entity_type + '-' + entity_id + '-' + field_name + '-' + delta;
            var slickQuizFieldSettings = fieldSettings[field_name];
            initSlickQuiz(selector, slickQuizSettings, slickQuizFieldSettings, context);
          });
        });
      });
    });
  }

  /**
   * Init slick quiz.
   *
   * @param string element
   *   Slick quiz selector.
   * @param object slickQuizSettings
   *   Slick quiz settings.
   * @param object context
   *   Context.
   */
  function initSlickQuiz(element, slickQuizSettings, slickQuizFieldSettings, context) {
    $(element, context).once('slick-quiz').each(function () {
      if (slickQuizSettings != 'undefined' && slickQuizSettings != null) {
        slickQuizSettings.json = processQuiz(slickQuizSettings.json);
        var options = {
          json: slickQuizSettings.json,
          checkAnswerText:  Drupal.t('Check my answer!'),
          nextQuestionText: Drupal.t('Next') + ' &raquo;',
          questionCountText: Drupal.t('Question %current of %total'),
          preventUnansweredText: Drupal.t('You must select at least one answer.'),
          questionTemplateText:  '%count. %text',
          scoreTemplateText: '%score/%total',
          nameTemplateText:  '',
          tryAgainText: Drupal.t('Try again')
        };
        options = $.extend(options, slickQuizFieldSettings);
        $(this).slickQuiz(options);
      }
    });
  }

  /**
   * Set default values to slick quiz.
   *
   * @param object slickQuizSettings
   *   Settings.
   */
  function processQuiz(slickQuizSettings) {
    var slickQuiz = {
      'questions': slickQuizSettings.questions,
      'info': slickQuizSettings.info
    };
    slickQuiz.info.name = '';
    slickQuiz.info.main = '';
    for (var i = 0; i < slickQuiz.questions.length; i++) {
      if (typeof(slickQuiz.questions[i].correct) == 'undefined' || slickQuiz.questions[i].correct == null) {
        slickQuiz.questions[i].correct = '';
      }
      if (typeof(slickQuiz.questions[i].incorrect) == 'undefined' || slickQuiz.questions[i].incorrect == null) {
        slickQuiz.questions[i].incorrect = '';
      }
    }
    return slickQuiz;
  }

})(jQuery, Drupal);
